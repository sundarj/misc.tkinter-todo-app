import tkinter as tk
import tkinter.messagebox as messagebox
import random
import string
import json
import os.path

SAVE_FILE_NAME = "todos.json"

def random_id():
    return "".join(random.choices(string.ascii_letters, k=10))

def main():
    if os.path.isfile(SAVE_FILE_NAME):
        with open(SAVE_FILE_NAME, encoding="utf-8") as file:
            todos = json.load(file)
    else:
        todos = [{
            "id": random_id(),
            "text": "Buy milk",
            "done": False,
        }, {
            "id": random_id(),
            "text": "Write poem",
            "done": False,
        }]
    root = tk.Tk()
    entry = tk.Entry(root)
    entry.pack()
    def add_todo(event):
        text = entry.get()
        if not text:
            messagebox.showerror("Error", "Field is empty")
        else:
            todos.append({
                "id": random_id(),
                "text": text,
                "done": False,
            })
            entry.delete(0, tk.END)
    entry.bind("<Return>", add_todo)
    frame = tk.Frame(root)
    frame.pack()
    def toggle_todo(todo):
        for saved_todo in todos:
            if saved_todo["id"] == todo["id"]:
                saved_todo["done"] = not todo["done"]
    def render_todos():
        for child in frame.winfo_children():
            child.destroy()
        for todo in todos:
            checkbutton = tk.Checkbutton(
                frame,
                text=todo["text"],
                command=lambda todo=todo: toggle_todo(todo)
            )
            if todo["done"]:
                checkbutton.select()
            checkbutton.pack()
        frame.after(1000, render_todos)
    render_todos()
    def save_and_exit():
        with open(SAVE_FILE_NAME, "w", encoding="utf-8") as file:
            json.dump(todos, file)
        root.destroy()
    root.protocol("WM_DELETE_WINDOW", save_and_exit)
    root.mainloop()


if __name__ == '__main__':
    main()